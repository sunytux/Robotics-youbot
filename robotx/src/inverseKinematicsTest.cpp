#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <stdlib.h>
#include "inverseKinematics.h"
#include "inverseKinematics.cpp"

using namespace std;

bool checkOk(float val1, float val2, float val3, float val4, float val5)
{
	bool result = true;
	if (val1 > 169 || val1 < -169)
	{
		result = false;
	}
	else if (val2 > 90 || val2 < -65)
	{
		result = false;
	}
	else if (val3 > 146 || val3 < -151)
	{
		result = false;
	}
	else if (val4 > 102.5 || val4 < -102.5)
	{
		result = false;
	}
	else if (val5 > 167.5 || val5 < -167.5)
	{
		result = false;
	}
	return result;
}

int main()
{
	bool continueLoop = true;
	bool possible;
	char c;
	float p_X;
	float p_Y;
	float p_Z;
	float thetaP;
	float theta5;
	inverseKinematics testKinematics;
	float angle[5];
	
	while (continueLoop)
	{
		cout << endl << "****************************************" << endl << endl;
		cout << endl << "****************************************" << endl << endl;;
		cout << "Enter the value of: P_x in mm ";
		cin >> p_X;
		cout << "Enter the value of: P_y in mm ";
		cin >> p_Y;
		cout << "Enter the value of: P_z in mm ";
		cin >> p_Z;
		cout << "Enter the value of theta P of the end factor in degree";
		cin >> thetaP; thetaP = thetaP*M_PI / 180;
		cout << "Enter the value of theta 5 of the end factor in degree";
		cin >> theta5; theta5 = theta5*M_PI / 180;

		testKinematics.setPosition(p_X, p_Y, p_Z, thetaP, theta5);
		testKinematics.inverseKinematicsMain();
		testKinematics.angleHandle(angle);

		cout << "The angles found in inverse kinematics are:" << endl;
		cout << "theta_1 = " << angle[0] / M_PI * 180 << endl;
		cout << "theta_2 = " << angle[1] / M_PI * 180 << endl;
		cout << "theta_3 = " << angle[2] / M_PI * 180 << endl;
		cout << "theta_4 = " << angle[3] / M_PI * 180 << endl;
		cout << "theta_5 = " << angle[4] / M_PI * 180 << endl;

		possible=checkOk(angle[0] / M_PI * 180, angle[1] / M_PI * 180, angle[2] / M_PI * 180, angle[3] / M_PI * 180, angle[4] / M_PI * 180);

		if (possible)
		{
			cout << "This solution is possible" << endl;
		}
		else
		{
			cout << "This solution is impossible." << endl;
			cout << "The maximum possibilities for each angles are:" << endl;
			cout << "Theta 1 = [+169, -169]" << endl;
			cout << "Theta 2 = [+90, -65]" << endl;
			cout << "Theta 3 = [+146, -151]" << endl;
			cout << "Theta 4 = [+102.5, -102.5]" << endl;
			cout << "Theta 5 = [+167.5, -167.5]" << endl;
		}

		cout << "P_z = " << 155 * cos(angle[1]) + 135 * cos(angle[1] + angle[2]) + 170.76 * cos(angle[1] + angle[2] + angle[3]) << endl;
		cout << "4_z = " << 155 * cos(angle[1]) + 135 * cos(angle[1] + angle[2]) << endl;
		cout << endl << "Continue ? Y/n" << endl;
		cin >> c;
		if (c != 'Y' && c != 'y')
		{
			continueLoop = false;
		}
	}
}