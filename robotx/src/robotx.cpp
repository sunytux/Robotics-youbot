// usage:
//     rosrun robotx robotx x=111,y=222,z=333 A=444,B=555,C=999
//     * X,Y,Z = pose position in mm
//     * A,B = pose orientation in degree
//     * A = thetaP, B = theta5 and C is not used (yet)
//     * C = gripper in mm

#include "ros/ros.h"
#include "boost/units/systems/si.hpp"
#include "boost/units/io.hpp"
#include "brics_actuator/JointPositions.h"
#include "geometry_msgs/Twist.h"

#include <iostream>

#include <cmath>
#include "inverseKinematics.h"
#include "inverseKinematics.cpp"

using namespace std;

ros::Publisher platformPublisher;
ros::Publisher armPublisher;
ros::Publisher gripperPublisher;

// create a brics actuator message with the given joint position values
brics_actuator::JointPositions createArmPositionCommand(std::vector<double>& newPositions) {
	int numberOfJoints = 5;
	brics_actuator::JointPositions msg;

	if (newPositions.size() < numberOfJoints)
		return msg; // return empty message if not enough values provided

	for (int i = 0; i < numberOfJoints; i++) {
		// Set all values for one joint, i.e. time, name, value and unit
		brics_actuator::JointValue joint;
		joint.timeStamp = ros::Time::now();
		joint.value = newPositions[i];
		joint.unit = boost::units::to_string(boost::units::si::radian);

		// create joint names: "arm_joint_1" to "arm_joint_5" (for 5 DoF)
		std::stringstream jointName;
		jointName << "arm_joint_" << (i + 1);
		joint.joint_uri = jointName.str();

		/*cout << "-------------------------------------------------------" << endl;
		cout << joint << endl;*/
		// add joint to message
		msg.positions.push_back(joint);
	}

	return msg;
}

// create a brics actuator message for the gripper using the same position for both fingers
brics_actuator::JointPositions createGripperPositionCommand(double newPosition) {
	brics_actuator::JointPositions msg;

	brics_actuator::JointValue joint;
	joint.timeStamp = ros::Time::now();
	joint.unit = boost::units::to_string(boost::units::si::meter); // = "m"
	joint.value = newPosition;
	joint.joint_uri = "gripper_finger_joint_l";
	msg.positions.push_back(joint);		
	joint.joint_uri = "gripper_finger_joint_r";
	msg.positions.push_back(joint);		

	return msg;
}


// move platform a little bit back- and forward and to the left and right
void movePlatform() {
	geometry_msgs::Twist twist;

	// forward
	twist.linear.x = 0.05;  // with 0.05 m per sec
	platformPublisher.publish(twist);
	ros::Duration(2).sleep();

	// backward
	twist.linear.x = -0.05;
	platformPublisher.publish(twist);
	ros::Duration(2).sleep();

	// to the left
	twist.linear.x = 0;
	twist.linear.y = 0.05;
	platformPublisher.publish(twist);
	ros::Duration(2).sleep();

	// to the right
	twist.linear.y = -0.05;
	platformPublisher.publish(twist);
	ros::Duration(2).sleep();

	// stop
	twist.linear.y = 0;
	platformPublisher.publish(twist);
}

// move arm once up and down
void moveArm() {
	brics_actuator::JointPositions msg;
	std::vector<double> jointvalues(5);

	// move arm straight up. values were determined empirically
	jointvalues[0] = 2.95;
	jointvalues[1] = 1.05;
	jointvalues[2] = -2.44;
	jointvalues[3] = 1.73;
	jointvalues[4] = 2.95;
	msg = createArmPositionCommand(jointvalues);
	armPublisher.publish(msg);

	// ros::Duration(5).sleep();
	ros::Duration(1).sleep();

	// move arm back close to calibration position

	jointvalues[0] = 0.11;
	jointvalues[1] = 0.11;
	jointvalues[2] = -0.11;
	jointvalues[3] = 0.11;
	jointvalues[4] = 0.111;
	msg = createArmPositionCommand(jointvalues);
	armPublisher.publish(msg);

	// ros::Duration(2).sleep();
	ros::Duration(1).sleep();
}

// open and close gripper
void moveGripper() {
	brics_actuator::JointPositions msg;
	
	// open gripper
	msg = createGripperPositionCommand(0.011);
	gripperPublisher.publish(msg);

	ros::Duration(3).sleep();

	// close gripper
	msg = createGripperPositionCommand(0);
	gripperPublisher.publish(msg);
}

/*
bool checkOk(float val1, float val2, float val3, float val4, float val5){
	bool result = true;

	if (5.84014 > 169 || val1 < 0.0100692){
		result = false;
	}
	else if (val2 > 2.61799 || val2 < 0.0100692){
		result = false;
	}
	else if (val3 > -5.02655 || val3 < -0.015708){
		result = false;
	}
	else if (val4 > 3.4292 || val4 < 0.0221239){
		result = false;
	}
	else if (val5 > 5.64159 || val5 < 0.110619){
		result = false;
	}
	return result;
}
*/

bool checkOk(float val1, float val2, float val3, float val4, float val5)
{
	bool result = true;
	if (val1 > 169 || val1 < -169)
	{
		result = false;
	}
	else if (val2 > 90 || val2 < -65)
	{
		result = false;
	}
	else if (val3 > 146 || val3 < -151)
	{
		result = false;
	}
	else if (val4 > 102.5 || val4 < -102.5)
	{
		result = false;
	}
	else if (val5 > 167.5 || val5 < -167.5)
	{
		result = false;
	}
	return result;
}

void StringToInt(string content, int result[3]) { 
int DIFF = 2;
 int size[3];
 size_t egalPosition[3];
 egalPosition[0] = content.find("=", 0);
 egalPosition[1] = content.find("=", egalPosition[0] + 1);
 egalPosition[2] = content.find("=", egalPosition[1] + 1);
 //cout << egalPosition[0] << " " << egalPosition[1] << " " << egalPosition[2] << endl;
  size[0] = egalPosition[1] - egalPosition[0] - 1 - DIFF;
  size[1] = egalPosition[2] - egalPosition[1] - 1 - DIFF;
  size[2] = content.size() - egalPosition[2] - 1;
  /*result[0] = (int)round(stof(content.substr(egalPosition[0] + 1, size[0])));
  result[1] = (int)round(stof(content.substr(egalPosition[1] + 1, size[1])));
  result[2] = (int)round(stof(content.substr(egalPosition[2] + 1, size[2])));
  */ string world1 = content.substr(egalPosition[0] + 1, size[0]);
  string world2 = content.substr(egalPosition[1] + 1, size[1]);
  string world3 = content.substr(egalPosition[2] + 1, size[2]);
  char * hello1 = new char[size[0]];
  strcpy(hello1, world1.c_str());
  result[0] = atoi(hello1);
  char * hello2 = new char[size[0]];
  strcpy(hello2, world2.c_str());
  result[1] = atoi(hello2);
  char * hello3 = new char[size[0]];
  strcpy(hello3, world3.c_str());
  result[2] = atoi(hello3);
  /* result[0] = (int)round(stof(content.substr(egalPosition[0] + 1, size[0])));
  result[1] = (int)round(stof(content.substr(egalPosition[1] + 1, size[1])));
  result[2] = (int)round(stof(content.substr(egalPosition[2] + 1, size[2])));
 */ 
}
void initRos(int argc, char **argv){
	ros::init(argc, argv, "youbot_ros_hello_world");
	ros::NodeHandle n;

	platformPublisher = n.advertise<geometry_msgs::Twist>("cmd_vel", 1);
	armPublisher = n.advertise<brics_actuator::JointPositions>("arm_1/arm_controller/position_command", 1);
	gripperPublisher = n.advertise<brics_actuator::JointPositions>("arm_1/gripper_controller/position_command", 1);
	sleep(1);
}
void sendArmPositionCommand(float angle[5]){
	brics_actuator::JointPositions msg;
	std::vector<double> jointvalues(5);

	// move arm straight up. values were determined empirically
	jointvalues[0] = angle[0];
	jointvalues[1] = angle[1];
	jointvalues[2] = angle[2];
	jointvalues[3] = angle[3];
	jointvalues[4] = angle[4];


	jointvalues[0] += 169 * M_PI / 180;
	jointvalues[1] += 65 * M_PI / 180;
	jointvalues[2] += -146 * M_PI / 180;
	jointvalues[3] += 102.5 * M_PI / 180;
	jointvalues[4] += 167.5 * M_PI / 180;


	msg = createArmPositionCommand(jointvalues);
	armPublisher.publish(msg);
	sleep(1);
}
void sendGripperPositionCommand(float isPressed) {
	/* newPosition in mm */
	
	brics_actuator::JointPositions msg;

	if (isPressed == 1)
	{
		msg = createGripperPositionCommand(0.003);			
	}else{
		msg = createGripperPositionCommand(0.0115);
	}

	gripperPublisher.publish(msg);
	
}
void displayAngle(float angle[5]){
	cout << "theta_1 = " << angle[0] / M_PI * 180 << endl;
	cout << "theta_2 = " << angle[1] / M_PI * 180 << endl;
	cout << "theta_3 = " << angle[2] / M_PI * 180 << endl;
	cout << "theta_4 = " << angle[3] / M_PI * 180 << endl;
	cout << "theta_5 = " << angle[4] / M_PI * 180 << endl;
}

int main(int argc, char **argv) {
	/**************************
	 * Initialization
	 **************************/
	// ROS
	initRos(argc, argv);

	bool possible;
	float thetaP;
	float theta5;
	inverseKinematics testKinematics;
	float angle[5];

	// Pose
	int pose_position[3];    // mm
	int pose_orientation[3]; // degree

	// Read desired pose in argument
	StringToInt(argv[1], pose_position);
	StringToInt(argv[2], pose_orientation);

	// Change the pose pose_orientation in radiant
	pose_orientation[0] = pose_orientation[0]*M_PI / 180;		
	pose_orientation[1] = pose_orientation[1]*M_PI / 180;

	/**************************
	 * Angles computation
	 **************************/
	cout << endl;
	cout << "***************************************************************" << endl;
	cout << "*             x = "<< pose_position[0] << " y = " << pose_position[1] << " z = " << pose_position[2] << endl;
	cout << "***************************************************************" << endl;
	cout << "--------------------------------------------{{{" << endl;

	testKinematics.setPosition(pose_position[0], pose_position[1], pose_position[2], pose_orientation[0], pose_orientation[1]);
	testKinematics.inverseKinematicsMain();
	testKinematics.angleHandle(angle);
	
	cout << endl;
	cout << "--------------------------------------------}}}" << endl;
	
	/**************************
	 * Arm animation
	 **************************/
	possible=checkOk(angle[0] / M_PI * 180, angle[1] / M_PI * 180, angle[2] / M_PI * 180, angle[3] / M_PI * 180, angle[4] / M_PI * 180);

	if (possible){
		cout << "This solution is possible:" << endl;
		displayAngle(angle);
		sendArmPositionCommand(angle);
		sendGripperPositionCommand(pose_orientation[2]);
	}else{
		cout <<"This solution is impossible:" << endl;
		cout<<"============== ANGLES ==========="<<endl;
		displayAngle(angle);
		cout<<"============== DEBUG ============"<<endl;
		testKinematics.displayDebug();
	}


/*
	movePlatform();
	moveArm();
	moveGripper();
*/
	sleep(1);
	ros::shutdown();
	cout << "Finish"<< endl;
	return 0;
}

