//#pragma once
#define _USE_MATH_DEFINES
#include "inverseKinematics.h"
#include <cmath>
#include <iostream>

/*
Use:
- Creation of the object using inverseKinematics(float pos_P_x, float pos_P_Y, float pos_P_Z, float thetaP, float theta5Angle)
- Call of inverseKinematicsMain() to make the calculation of the angles.
- Get the angle back using angleHandle(float output[5]).
*/
inverseKinematics::inverseKinematics()
{
	robotJointAngle_1 = 0.0f;
	robotJointAngle_2 = 0.0f;
	robotJointAngle_3 = 0.0f;
	robotJointAngle_4 = 0.0f;
	robotJointAngle_5 = 0.0f;
	position_X = 0.0f;
	position_Y = 0.0f;
	position_Z = 0.0f;

	for (int i = 0; i < 3; ++i)
	{
		Node_1[i] = 0;
	}
}

inverseKinematics::inverseKinematics(float pos_1, float pos_2, float pos_3, float thetaP, float theta5Angle)
{
	position_X = pos_1;
	position_Y = pos_2;
	position_Z = pos_3;

	robotJointAngle_1 = 0.0f;
	robotJointAngle_2 = 0.0f;
	robotJointAngle_3 = 0.0f;
	robotJointAngle_4 = thetaP;		//Wanted angle of the end factor
	robotJointAngle_5 = theta5Angle;

	for (int i = 0; i < 3; ++i)
	{
		Node_1[i] = 0;
	}
}

inverseKinematics::~inverseKinematics()
{

}


//**************************************************************
//Usefull functions

void inverseKinematics::offSetAngle()		//Finally action moved in the main().
{
	float offsetTheta1 = 169 * M_PI / 180;
	float offsetTheta2 = 65 * M_PI / 180;
	float offsetTheta3 = -146 * M_PI / 180;
	float offsetTheta4 = 102.5 * M_PI / 180;
	float offsetTheta5 = 167.5 * M_PI / 180;
	robotJointAngle_1 += offsetTheta1;
	robotJointAngle_2 += offsetTheta2;
	robotJointAngle_3 += offsetTheta3;
	robotJointAngle_4 += offsetTheta4;
	robotJointAngle_5 += offsetTheta5;
}

void inverseKinematics::setPosition(float pX, float pY, float pZ, float thetaP, float theta5Angle)
{
	position_X = pX;
	position_Y = pY;
	position_Z = pZ;
	robotJointAngle_4 = thetaP;
	robotJointAngle_5 = theta5Angle;
}

void inverseKinematics::setEndFactorAngle(float thetaP)
{
	robotJointAngle_4 = thetaP;
}

bool checkOkAngle(float val1, float val2, float val3, float val4, float val5)
{
	bool result = true;
	if (val1 > 169 * M_PI / 180 || val1 < -169 * M_PI / 180)
	{
		result = false;
	}
	else if (val2 > 90 * M_PI / 180 || val2 < -65 * M_PI / 180)
	{
		result = false;
	}
	else if (val3 > 146 * M_PI / 180 || val3 < -151 * M_PI / 180)
	{
		result = false;
	}
	else if (val4 > 102.5 * M_PI / 180 || val4 < -102.5 * M_PI / 180)
	{
		result = false;
	}
	else if (val5 > 167.5 * M_PI / 180 || val5 < -167.5 * M_PI / 180)
	{
		result = false;
	}
	return result;
}

void inverseKinematics::angleHandle(float output[5])
{
	output[0] = robotJointAngle_1;
	output[1] = robotJointAngle_2;
	output[2] = robotJointAngle_3;
	output[3] = M_PI / 2 + robotJointAngle_4 - robotJointAngle_3 - robotJointAngle_2;		//Real theta4, before theta4=thetaP.
	output[4] = robotJointAngle_5;
	if (!checkOkAngle(output[0], output[1], output[2], output[3], output[4]))				//Extra verification used during debug.
	{
		std::cout << "Issues might occur because the solution can't be used because of the limitations of the robot" << std::endl;
	}
	displayDebug();
}

float normCalculation(float a[3], float b[3])
{
	return sqrt((b[2] - a[2])*(b[2] - a[2]) + (b[1] - a[1])*(b[1] - a[1]) + (b[0] - a[0])*(b[0] - a[0]));
}

float norm2(float f1, float f2, float y1, float y2)
{
	return sqrt((f1 - y1)*(f1 - y1) + (f2 - y2)*(f2 - y2));
}
//Rotation around Z.
void inverseKinematics::node_2_Position()		//Ok verifier
{
	float xValue = 33;
	float yValue = 0;
	float zValue = 0;
	//No offset for the rotation
	Node_2[0] = Node_1[0] + xValue*cos(robotJointAngle_1) - yValue*sin(robotJointAngle_1);
	Node_2[1] = Node_1[1] + xValue*sin(robotJointAngle_1) + yValue*cos(robotJointAngle_1);
	Node_2[2] = Node_1[2] + zValue;
}

//Segment 2-P distance
void inverseKinematics::P2DistanceFct()
{
	P2PointsDistance = normCalculation(Node_2, Node_1);
}

//Node #4 position calculation
void inverseKinematics::node_4_Position()		//Depends on where is P for robotJointAngle_1
{
	float dist = 170.76*cos(robotJointAngle_4);
	Node_4[0] = Node_P[0] - dist*cos(robotJointAngle_1);
	Node_4[1] = Node_P[1] + dist*sin(robotJointAngle_1);
	Node_4[2] = Node_P[2] + 170.76*sin(robotJointAngle_4);
	std::cout << "theta 1 = " << robotJointAngle_1 / M_PI * 180 << " | theta 4 =" << robotJointAngle_4 / M_PI * 180 << "  |  distance =" << dist << std::endl;
}

float inverseKinematics::calculationPossibleAngle()
{
	float thetaTau = asin((Node_P[2] - Node_2[2]) / (normCalculation(Node_P, Node_2)));
	float b = 155 + 135;
	float a = 170.76;
	float c = normCalculation(Node_P, Node_2);
	float thetaBeta = acos((a*a + c*c - b*b) / (2 * a*c));
	std::cout << "**********************" << std::endl;
	std::cout << "a:" << a << "b:" << b << "c:" << c << std::endl;
	std::cout << "Theta tau = " << thetaTau / M_PI * 180 << "Theta Beta = " << thetaBeta / M_PI * 180 << std::endl;
	std::cout << "**********************" << std::endl;
	return thetaBeta - thetaTau;
}

void inverseKinematics::angleThetaPVerification()
{
	int enter;
	bool solutionPossible = false;
	std::cout << "Distance node 2 and node P: " << normCalculation(Node_P, Node_2) << std::endl;
	std::cout << "Distance node 2 and node 4: " << normCalculation(Node_4, Node_2) << std::endl;
	std::cout << "Distance node 4 and node P: " << normCalculation(Node_P, Node_4) << std::endl;
	while (solutionPossible == false)
	{
		std::cout << "P_x: " << Node_P[0] << " | P_y: " << Node_P[1] << " | P_z: " << Node_P[2] << std::endl;

		float dist24 = normCalculation(Node_4, Node_2);
		if (dist24 > (135 + 155))
		{
			if (normCalculation(Node_P, Node_2) > (155 + 135 + 170.76))
			{
				std::cout << "It is impossible to find a solution with this position and angle." << std::endl;
				std::cout << "Max is " << 155 + 135 + 170.76 << " and the actual value is " << normCalculation(Node_P, Node_2) << std::endl;
				std::cout << "For P_x:" << std::endl;
				std::cin >> enter;
				position_X = enter;
				std::cout << "For P_y:" << std::endl;
				std::cin >> enter;
				position_Y = enter;
				std::cout << "For P_z:" << std::endl;
				std::cin >> enter;
				position_Z = enter;
				movePinReference1();			//Translation of P in the frame 1.
				std::cout << "New values are: " << Node_P[0] << " " << Node_P[1] << " " << Node_P[2] << std::endl;
				calcAngle_1();					//Calculation of theta_1.
				node_2_Position();				//Calculation of the position of the node  #2.
				P2DistanceFct();				//Calculation of the distance between points P and #2.
				node_4_Position();				//Calculation of the position of the node #4.
				displayDebug();
			}
			else
			{
				std::cout << "It is impossible to find a solution with this angle." << std::endl;
				std::cout << "Proposition: " << calculationPossibleAngle() * 180 / M_PI << std::endl;
				std::cout << "New theta P in �:" << std::endl;
				// std::cin >> enter;
				std::cout << robotJointAngle_4 * 180 / M_PI<< std::endl;
				robotJointAngle_4=calculationPossibleAngle();
				// robotJointAngle_4 = enter*M_PI / 180;
				node_2_Position();				//Calculation of the position of the node  #2.
				P2DistanceFct();				//Calculation of the distance between points P and #2.
				node_4_Position();				//Calculation of the position of the node #4.
				displayDebug();
			}
		}
		else
		{
			std::cout << "Solution possible" << std::endl;
			solutionPossible = true;
		}
	}
}

void inverseKinematics::calcAngle_1()		//Ok verifier
{
	// robotJointAngle_1 = abs(atan2(Node_P[1] - Node_1[1], Node_P[0] - Node_1[0]));
	robotJointAngle_1 = atan2(Node_P[1] - Node_1[1], Node_P[0] - Node_1[0]);
	
	if (robotJointAngle_1<0) {
		robotJointAngle_1=-robotJointAngle_1;
	}
	if (Node_P[1] > 0)
	{
		robotJointAngle_1 = -robotJointAngle_1;
	}
	std::cout<<robotJointAngle_1<<std::endl;
	std::cout<<atan2(Node_P[1] - Node_1[1], Node_P[0] - Node_1[0])<<std::endl;
	std::cout<<abs(atan2(Node_P[1] - Node_1[1], Node_P[0] - Node_1[0]))<<std::endl;
}

void inverseKinematics::calcAngle_2_and_3()
{
	std::cout << std::endl << " ****************************************** " << std::endl;
	std::cout << " Calculating theta 2 and theta 3 " << std::endl;
	std::cout << std::endl << " ****************************************** " << std::endl;
	float dist24 = normCalculation(Node_4, Node_2);
	float varXY = norm2(Node_2[0], Node_2[1], Node_4[0], Node_4[1]);
	float cosTheta3 = (varXY * varXY + (Node_4[2] - Node_2[2])*(Node_4[2] - Node_2[2]) - 135 * 135 - 155 * 155) / (2 * 155 * 135);
	std::cout << "cosTheta3 :" << cosTheta3 << std::endl;
	std::cout << "Theta3 :" << acos(cosTheta3) * 180 / M_PI << std::endl;
	if (abs(cosTheta3) > 1)
	{
		std::cout << "ERROR, costTheta3 too high." << std::endl;
		std::cout << " dist24 : " << dist24 << std::endl;
		std::cout << " Node_4[2] : " << Node_4[2] << std::endl;
		std::cout << " Node_2[2] : " << Node_2[2] << std::endl;
	}
	float sinTheta3_1 = sqrt(1 - cosTheta3*cosTheta3);
	float sinTheta3_2 = -sinTheta3_1;

	//Choice of theta3 according to the less movement needed on theta3 only.
	float tempAngle3_1 = atan2(sinTheta3_1, cosTheta3);
	float tempAngle3_2 = atan2(sinTheta3_2, cosTheta3);

	float tempAngle2_1 = M_PI / 2 + (atan2(Node_2[2] - Node_4[2], varXY) - atan2(135 * sin(tempAngle3_1), 155 + 135 * cos(tempAngle3_1)));
	float tempAngle2_2 = M_PI / 2 + (atan2(Node_2[2] - Node_4[2], varXY) - atan2(135 * sin(tempAngle3_2), 155 + 135 * cos(tempAngle3_2)));
	std::cout << "Available solutions: " << std::endl;
	std::cout << "theta 2 = " << tempAngle2_1 / M_PI * 180 << " and theta 3 = " << tempAngle3_1 / M_PI * 180 << std::endl;
	std::cout << "theta 2 = " << tempAngle2_2 / M_PI * 180 << " and theta 3 = " << tempAngle3_2 / M_PI * 180 << std::endl;

	float var1 = abs(tempAngle2_1 - robotJointAngle_2) + abs(tempAngle3_1 - robotJointAngle_3);
	float var2 = abs(tempAngle2_2 - robotJointAngle_2) + abs(tempAngle3_2 - robotJointAngle_3);

	if (checkOkAngle(0, tempAngle2_1, tempAngle3_1, 0, 0) && checkOkAngle(0, tempAngle2_2, tempAngle3_2, 0, 0))
	{
		//Choice of the position allowing the lowest difference with the actual position.
		if (var1 < var2)
		{
			robotJointAngle_3 = tempAngle3_1;
			robotJointAngle_2 = tempAngle2_1;
		}
		else
		{
			robotJointAngle_3 = tempAngle3_2;
			robotJointAngle_2 = tempAngle2_2;
		}
	}
	else if (checkOkAngle(0, tempAngle2_1, tempAngle3_1, 0, 0))
	{
		robotJointAngle_3 = tempAngle3_1;
		robotJointAngle_2 = tempAngle2_1;
	}
	else if (checkOkAngle(0, tempAngle2_2, tempAngle3_2, 0, 0))
	{
		robotJointAngle_3 = tempAngle3_2;
		robotJointAngle_2 = tempAngle2_2;
	}
	else
	{
		std::cout << "Can't find a solution for theta2 and theta3" << std::endl;
		robotJointAngle_3 = tempAngle3_2;
		robotJointAngle_2 = tempAngle2_2;
	}
	std::cout << std::endl << " ****************************************** " << std::endl;
	std::cout << " Finished calculating theta 2 and 3 " << std::endl;
	std::cout << std::endl << " ****************************************** " << std::endl << std::endl;
}

void inverseKinematics::displayDebug()
{
	Node_3[0] = Node_2[0] + 155 * cos(robotJointAngle_1);
	Node_3[1] = Node_2[1] + 155 * sin(robotJointAngle_1);
	Node_3[2] = Node_2[2] + 155 * sin(robotJointAngle_2);
	std::cout << "Node 1: (" << Node_1[0] << "," << Node_1[1] << "," << Node_1[2] << ")" << std::endl;
	std::cout << "Node 2: (" << Node_2[0] << "," << Node_2[1] << "," << Node_2[2] << ")" << std::endl;
	std::cout << "Node 3: (" << Node_3[0] << "," << Node_3[1] << "," << Node_3[2] << ")" << std::endl;
	std::cout << "Node 4: (" << Node_4[0] << "," << Node_4[1] << "," << Node_4[2] << ")" << std::endl;
	std::cout << "Node P: (" << Node_P[0] << "," << Node_P[1] << "," << Node_P[2] << ")" << std::endl;
	std::cout << "P_Z : " << Node_1[2] + Node_2[2] + 155 * cos(robotJointAngle_2) + 135 * cos(robotJointAngle_2 + robotJointAngle_3) + 170.6 * cos(robotJointAngle_2 + robotJointAngle_3 + robotJointAngle_4);
}

void inverseKinematics::inverseKinematicsMain()
{
	movePinReference1();			//Translation of P in the frame 1.							OK
	calcAngle_1();					//Calculation of theta_1.									OK
	node_2_Position();				//Calculation of the position of the node  #2.				OK
	P2DistanceFct();				//Calculation of the distance between points P and #2.		OK
	node_4_Position();				//Calculation of the position of the node #4.				OK
	displayDebug();

	angleThetaPVerification();		//Verification of the existence of a solution.				OK
	calcAngle_2_and_3();			//Calculation of the angle theta 2 and theta 3.				OK
	//offSetAngle();					//Take in account the offset for the angle.					OK
	displayDebug();

	//Theta 4 and Theta 5 are already set by the user.
}

void inverseKinematics::movePinReference1()
{
	float translation1ToPlate[3];	//[8, 0, 115];
	translation1ToPlate[0] = 0;
	translation1ToPlate[1] = 0;
	translation1ToPlate[2] = 115;	//Armjoint 1
	float Z_diff = 84;

	work_position_X = position_X - translation1ToPlate[0];
	work_position_Y = position_Y - translation1ToPlate[1];
	work_position_Z = position_Z - translation1ToPlate[2] - Z_diff;
	Node_P[0] = work_position_X;
	Node_P[1] = work_position_Y;
	Node_P[2] = work_position_Z;
}
