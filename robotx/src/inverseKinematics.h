//#pragma once
#ifndef INVERSEKINEMATICS_H
#define INVERSEKINEMATICS_H

/*
Use:
- Creation of the object using inverseKinematics(float pos_P_x, float pos_P_Y, float pos_P_Z, float thetaP, float theta5Angle)
- Call of inverseKinematicsMain() to make the calculation of the angles.
- Get the angle back using angleHandle(float output[5]).
It is possible to set new positions values using setPosition(float pX, float pY, float pZ, float thetaP, float theta5Angle)
*/

class inverseKinematics
{
public:
	//Basic functions
	inverseKinematics();
	inverseKinematics(float, float, float, float, float);
	~inverseKinematics();

	//Payload
//public:
	void setPosition(float, float, float, float, float);
	void angleHandle(float[5]);
	void setEndFactorAngle(float);
	void inverseKinematicsMain();
	void displayDebug();
	void offSetAngle();

private:
	void calcAngle_1();
	void calcAngle_2_and_3();
	void movePinReference1();
	void node_2_Position();
	void P2DistanceFct();
	void node_4_Position();
	void angleThetaPVerification();
	float calculationPossibleAngle();

//private:
	float robotJointAngle_1;
	float robotJointAngle_2;
	float robotJointAngle_3;
	float robotJointAngle_4;
	float robotJointAngle_5;

	//P relative position to the center of the plate
	float position_X;
	float position_Y;
	float position_Z;

	//P relative position to the node 1
	float work_position_X;
	float work_position_Y;
	float work_position_Z;

	//Node position
	float Node_1[3];
	float Node_P[3];
	float Node_2[3];
	float Node_3[3];
	float Node_4[3];

	//Segment Length
	float P2PointsDistance;
};

#endif