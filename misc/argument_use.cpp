// Sample of cpp code to use arguments
// Compile:
//     g++ foo.c -o foo.out
// execute:
// 	   ./foo.out
#include <iostream>
 
int main(int argc, char *argv[])
{
    // argc = nb of arguments
    
    /* std::cout << "There are " << argc << " arguments:\n";
    for (int count=0; count < argc; ++count)
        std::cout << count << " " << argv[count] << '\n'; */

 
 	std::cout << "Position: \n";
 	std::cout << "    x: " << argv[1] << "\n";
 	std::cout << "    y: " << argv[2] << "\n";

    return 0;
}