# Install depedencies
# ubuntu:
# 	sudo apt-get install python-tk python-imaging-tk python3-tk
# note: 
# 	* bindable event:
# 		<ButtonPress-1>
# 		<ButtonRease-2>
# 		<Motion>
# 		...

import Tkinter as tk
import os

HEIGHT, WIDTH = 500, 500
RADIUS = 10

def main():
	root = tk.Tk()
	drawing_area = tk.Canvas(root)
	
	# Size of the window
	root.geometry('{}x{}'.format(HEIGHT, WIDTH))
	root.resizable(width=False, height=False)
	drawing_area.config(width=WIDTH, height=HEIGHT)
	

	def click(event):
		# print('{}, {}'.format(event.x, event.y))
		os.system("./argument_use.out {} {}".format(event.x, event.y))
		drawing_area.create_oval(event.x, event.y, event.x, event.y,width=RADIUS)
	
	# binding the events
	drawing_area.bind("<ButtonPress-1>", click)

	# start the GUI
	drawing_area.pack() 
	root.mainloop()

if __name__ == '__main__':
	main()