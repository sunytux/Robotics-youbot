#include <iostream>
#include <string>
#include <cmath>

using namespace std;

void StringToInt(string content, int result[3]) {
	int DIFF = 2;
	int size[3];
	size_t egalPosition[3];

	egalPosition[0] = content.find("=", 0);
	egalPosition[1] = content.find("=", egalPosition[0] + 1);
	egalPosition[2] = content.find("=", egalPosition[1] + 1);
	//cout << egalPosition[0] << " " << egalPosition[1] << " " << egalPosition[2] << endl;
	
	size[0] = egalPosition[1] - egalPosition[0] - 1 - DIFF;
	size[1] = egalPosition[2] - egalPosition[1] - 1 - DIFF;
	size[2] = content.size() - egalPosition[2] - 1;
	//cout << size[0] << " " << size[1] << " " << size[2] << endl;

	result[0] = (int)round(stof(content.substr(egalPosition[0] + 1, size[0])));
	result[1] = (int)round(stof(content.substr(egalPosition[1] + 1, size[1])));
	result[2] = (int)round(stof(content.substr(egalPosition[2] + 1, size[2])));
}
void StringToInt(string content, int result[3]) { int DIFF = 2; int size[3]; size_t egalPosition[3]; egalPosition[0] = content.find("=", 0); egalPosition[1] = content.find("=", egalPosition[0] + 1); egalPosition[2] = content.find("=", egalPosition[1] + 1); //cout << egalPosition[0] << " " << egalPosition[1] << " " << egalPosition[2] << endl; size[0] = egalPosition[1] - egalPosition[0] - 1 - DIFF; size[1] = egalPosition[2] - egalPosition[1] - 1 - DIFF; size[2] = content.size() - egalPosition[2] - 1; /*result[0] = (int)round(stof(content.substr(egalPosition[0] + 1, size[0]))); result[1] = (int)round(stof(content.substr(egalPosition[1] + 1, size[1]))); result[2] = (int)round(stof(content.substr(egalPosition[2] + 1, size[2]))); */ string world1 = content.substr(egalPosition[0] + 1, size[0]); string world2 = content.substr(egalPosition[1] + 1, size[1]); string world3 = content.substr(egalPosition[2] + 1, size[2]); char * hello1 = new char[size[0]]; strcpy(hello1, world1.c_str()); result[0] = atoi(hello1); char * hello2 = new char[size[0]]; strcpy(hello2, world2.c_str()); result[1] = atoi(hello2); char * hello3 = new char[size[0]]; strcpy(hello3, world3.c_str()); result[2] = atoi(hello3); /* result[0] = (int)round(stof(content.substr(egalPosition[0] + 1, size[0]))); result[1] = (int)round(stof(content.substr(egalPosition[1] + 1, size[1]))); result[2] = (int)round(stof(content.substr(egalPosition[2] + 1, size[2])));*/ }
int main() {
	string test = "x=123,y=456,z=789";
	int result[3];
	StringToInt(test, result);
	cout << result[0] << " " << result[1] << " " << result[2] << endl;
	string testBis = "x=-123.4,y=4.56,z=-45.789";
	StringToInt(testBis, result);
	cout << result[0] << " " << result[1] << " " << result[2] << endl;
	// system("pause");
}