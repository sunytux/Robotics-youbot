# New package
cd ~/catkin_ws/src
catkin_create_pkg robotx 

comment "find_package(catkin REQUIRED)"
copy find_package line
copy include_directories
uncomment add_executable line
uncomment target_link_libraries
uncomment "install(TARGETS robotx robotx_node" line
remove "_node" every occurance 

#cd ~/catkin_ws && catkin_make

export ROS_MASTER_URI=http://192.168.1.146:11311
export ROS_HOSTNAME=192.168.1.188 

rosrun robotx robotx
#rosrun youbot_ros_hello_world youbot_ros_hello_world 

