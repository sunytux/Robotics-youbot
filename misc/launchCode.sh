#!/bin/bash

# change the environement variable to a new IP (default localhost)

if [[ $# -eq 0 ]]; then
	ip="localhost:11311"
else
	ip=$1
fi

ROS_MASTER_URI=http://$ip
ROS_HOSTNAME=`hostname --ip-address`

export ROS_MASTER_URI
export ROS_HOSTNAME

`rosrun object_displacer object_displacer`
