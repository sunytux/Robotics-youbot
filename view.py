 #!/usr/bin/python
import Tkinter as tk
import os,json, time, math

SETTINGS={}

def loadSettings():
	settings = json.loads(open("settings.json").read())
	settings["frame_left_width"]   = settings["canvas_width"] 
	settings["frame_left_height"]  = settings["canvas_height"]
	settings["frame_right_width"]  = settings["width"]-settings["frame_left_width"]
	settings["frame_right_height"] = settings["canvas_height"]
	return settings

def rosrun(settings, param):
	# Environment variable used by ros
	cmd  =         "export ROS_MASTER_URI="+settings["ROS_MASTER_URI"]
	cmd += " &&\n"+"export ROS_HOSTNAME="+settings["ROS_HOSTNAME"]
	# launch the ros node
	cmd += " &&\n"+"rosrun "+settings["node_name"]+" "+settings["pkg_name"] 
	# Add its arguments
	cmd += " "+param
	
	# print param
	os.system(cmd)

def scenario_heart():
	coord = []
	paramItem = 10
	for i in range(1,paramItem):
		t = i * 2 * math.pi / paramItem;
		x = 300
		y = 120 / 16 * (16 * math.sin(t)*math.sin(t)*math.sin(t));
		z = 120 / 16 * (13 * math.cos(t) - 5 * math.cos(2 * t) - 2 * math.cos(3 * t) - math.cos(4 * t))+250;
		theta1 = 0
		theta2 = 0
		grip = SETTINGS["grip_released"]
		point = [x,y,z,theta1,theta2,grip]
		point = map(lambda i: "%.2f" % round(float(i),2),point)
		coord.append(point)

	return coord

def scenario_fetch(i):
	coord = json.loads(open("scenario.json").read())
	coord = coord[str(i)]
	point2str = lambda l: map(lambda i: "%.2f" % round(float(i),2),l)
	return map(point2str, coord)
	

def main():
	# ***********************
	# Init every element
	# ***********************
	# main window
	root = tk.Tk()
	
	# frame to place other object
	frame_right     = tk.Frame(root)
	frame_left      = tk.Frame(root)
	frame_top_right = tk.Frame(frame_right)
	frame_middle_right = tk.Frame(frame_right)
	frame_bottom_right = tk.Frame(frame_right)
	
	# different objects
	drawing_area = tk.Canvas(frame_left)
	legend_label = tk.Label(frame_left)

	Title_label  = tk.Label(frame_top_right)
	
	Info_label   = tk.Label(frame_middle_right)
	x_entry      = tk.Entry(frame_middle_right)
	y_entry      = tk.Entry(frame_middle_right)
	# y_entry      = tk.Entry(frame_middle_right)
	x_label		 = tk.Label(frame_middle_right)
	y_label		 = tk.Label(frame_middle_right)
	z_label		 = tk.Label(frame_middle_right)
	z_slider     = tk.Scale(frame_middle_right)
	scenar_label   = tk.Label(frame_middle_right)
	
	scenar_buttons = map(lambda i: tk.Button(frame_middle_right), range(10))

	master_label = tk.Label(frame_bottom_right)
	host_label	 = tk.Label(frame_bottom_right)
	master_entry = tk.Entry(frame_bottom_right)
	host_entry	 = tk.Entry(frame_bottom_right)
	send_button  = tk.Button(frame_bottom_right)
	grip_button  = tk.Button(frame_bottom_right)


	# ***********************
	# Cosmetic 
	# ***********************
	frame_left.config(background=SETTINGS["main_bg"])
	frame_right.config(background=SETTINGS["main_bg"])
	frame_top_right.config(background=SETTINGS["main_bg"])
	frame_middle_right.config(background=SETTINGS["main_bg"])
	frame_bottom_right.config(background=SETTINGS["main_bg"])
	root.configure(background=SETTINGS["main_bg"])
	drawing_area.config(background=SETTINGS["canvas_bg"],highlightthickness=0)

	Title_label.config(text=SETTINGS["title"], font="bold 16",background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	drawing_area.create_oval(int(SETTINGS["canvas_width"])/2, int(SETTINGS["canvas_height"])/2, int(SETTINGS["canvas_width"])/2, int(SETTINGS["canvas_height"])/2,width=SETTINGS["dot_radius"],outline=SETTINGS["color3"])
	legend_label.config(text="y\n     x",anchor=tk.W, justify=tk.LEFT,bg=SETTINGS["canvas_bg"])
	
	Info_label.config(text="",background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	
	x_label.config(text="x:", anchor=tk.E, justify=tk.RIGHT,background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	y_label.config(text="y:", anchor=tk.E, justify=tk.RIGHT,background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	z_label.config(text="z:", anchor=tk.E, justify=tk.RIGHT,background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	scenar_label.config(text="Scenario",background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	
	for i in range(len(scenar_buttons)):
		scenar_buttons[i].config(text=str(i),relief="solid",bg=SETTINGS["main_bg"],fg=SETTINGS["main_fg"],font="bold 16",highlightthickness=0,borderwidth=0)
	scenar_buttons[1].config(fg=SETTINGS["color1"],activebackground=SETTINGS["color1"],activeforeground=SETTINGS["main_fg"])
	scenar_buttons[2].config(fg=SETTINGS["color2"],activebackground=SETTINGS["color2"],activeforeground=SETTINGS["main_fg"])
	scenar_buttons[3].config(fg=SETTINGS["color3"],activebackground=SETTINGS["color3"],activeforeground=SETTINGS["main_fg"])
	scenar_buttons[4].config(fg=SETTINGS["color2"],activebackground=SETTINGS["color2"],activeforeground=SETTINGS["main_fg"])
	scenar_buttons[5].config(fg=SETTINGS["color3"],activebackground=SETTINGS["color3"],activeforeground=SETTINGS["main_fg"])
	scenar_buttons[6].config(fg=SETTINGS["color1"],activebackground=SETTINGS["color1"],activeforeground=SETTINGS["main_fg"])
	scenar_buttons[7].config(fg=SETTINGS["color3"],activebackground=SETTINGS["color3"],activeforeground=SETTINGS["main_fg"])
	scenar_buttons[8].config(fg=SETTINGS["color1"],activebackground=SETTINGS["color1"],activeforeground=SETTINGS["main_fg"])
	scenar_buttons[9].config(fg=SETTINGS["color2"],activebackground=SETTINGS["color2"],activeforeground=SETTINGS["main_fg"])


	x_entry.insert(0,"0")
	y_entry.insert(0,"0")
	x_entry.config(background=SETTINGS["entry_bg"], fg=SETTINGS["entry_fg"],highlightthickness=0,borderwidth=0)
	y_entry.config(background=SETTINGS["entry_bg"], fg=SETTINGS["entry_fg"],highlightthickness=0,borderwidth=0)
	z_slider.config(from_=SETTINGS["y_min"], to=SETTINGS["y_max"], orient=tk.HORIZONTAL,background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	z_slider.set((SETTINGS["y_min"]+SETTINGS["y_max"])/2)

	master_label.config(text="Ros Master URI:",background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	host_label.config(text="Ros Hostname:",background=SETTINGS["main_bg"], fg=SETTINGS["main_fg"],highlightbackground=SETTINGS["main_bg"])
	master_entry.insert(0,SETTINGS["ROS_MASTER_URI"])
	host_entry.insert(0,SETTINGS["ROS_HOSTNAME"])
	master_entry.config(background=SETTINGS["entry_bg"], fg=SETTINGS["entry_fg"],highlightthickness=0,borderwidth=0)
	host_entry.config(background=SETTINGS["entry_bg"], fg=SETTINGS["entry_fg"],highlightthickness=0,borderwidth=0)
	send_button.config(text="Send",relief="solid",highlightthickness=0,borderwidth=0)
	grip_button.config(text="Grip",bg=SETTINGS["grip_buton_bg_release"],relief="solid",highlightthickness=0,borderwidth=0)

	# ***********************
	# Positioning 
	# ***********************
	# Main window
	root.title(SETTINGS["title"])
	root.geometry('{}x{}'.format(SETTINGS["width"], SETTINGS["height"])) # Size of the window
	root.resizable(width=False, height=False)

	# Sizing elements
	Info_label.config(width=1)
	x_entry.config(width=3)
	y_entry.config(width=3)
	# y_entry.config(width=3)
	
	frame_right.config(width=SETTINGS["frame_right_width"], height=SETTINGS["frame_right_height"])
	frame_left.config( width=SETTINGS["frame_left_width"],  height=SETTINGS["frame_right_height"])
	frame_top_right.config(   height=SETTINGS["topFrame_height"])
	frame_middle_right.config(height=SETTINGS["middleFrame_height"])
	frame_bottom_right.config(height=SETTINGS["bottomFrame_height"])
	
		# child of frame_left/right will not impose their size
	frame_left.pack_propagate(0)
 	frame_right.pack_propagate(0)
 	frame_top_right.pack_propagate(0)
 	frame_middle_right.pack_propagate(0)
 	frame_bottom_right.pack_propagate(0)
	

	# Placing the frames
	frame_left.pack(        side=tk.LEFT,  fill=tk.BOTH, expand=1,padx=SETTINGS["margin"],pady=SETTINGS["margin"])
	frame_right.pack(       side=tk.RIGHT, fill=tk.BOTH, expand=1,padx=SETTINGS["margin"], pady=SETTINGS["margin"])
	frame_top_right.pack(   side=tk.TOP,   fill=tk.X, expand=1)
	frame_middle_right.pack(      		   fill=tk.X, expand=1)
	frame_bottom_right.pack(side=tk.BOTTOM,fill=tk.X, expand=1)
	
	
	# Other objects
	drawing_area.pack(fill=tk.BOTH, expand=1)
	legend_label.place(x=5,y=SETTINGS["canvas_height"]-3,anchor=tk.SW)

	Title_label.pack(fill=tk.BOTH)
	
		# Object in middle frame
		# grid_columnconfigure : weight = 1 -> column will grow if space of weight 0 is available 
	frame_middle_right.grid_columnconfigure(0, weight=1)
	frame_middle_right.grid_columnconfigure(1, weight=1)
	frame_middle_right.grid_columnconfigure(2, weight=1)
	frame_middle_right.grid_columnconfigure(3, weight=1)
	frame_middle_right.grid_columnconfigure(4, weight=1)
	frame_middle_right.grid_columnconfigure(5, weight=1)
	Info_label.grid(column=0, row=0, columnspan=6, sticky=tk.W+tk.E+tk.N+tk.S)
	x_label.grid(   column=0, row=1, columnspan=1, sticky=tk.W+tk.E+tk.N+tk.S)
	x_entry.grid(   column=1, row=1, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S)
	y_label.grid(   column=3, row=1, columnspan=1, sticky=tk.W+tk.E+tk.N+tk.S)
	y_entry.grid(   column=4, row=1, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S)
	# y_entry.grid(   column=2, row=2, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S)
	z_label.grid(   column=0, row=2, columnspan=1, rowspan=2, sticky=tk.S)
	z_slider.grid(  column=1, row=2, columnspan=5, rowspan=2, sticky=tk.W+tk.E+tk.N+tk.S)
	scenar_label.grid(column=0, row=5, columnspan=6, sticky=tk.S)
	scenar_buttons[1].grid(column=0, row=6, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)
	scenar_buttons[2].grid(column=2, row=6, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)
	scenar_buttons[3].grid(column=4, row=6, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)
	scenar_buttons[4].grid(column=0, row=7, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)
	scenar_buttons[5].grid(column=2, row=7, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)
	scenar_buttons[6].grid(column=4, row=7, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)
	scenar_buttons[7].grid(column=0, row=8, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)
	scenar_buttons[8].grid(column=2, row=8, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)
	scenar_buttons[9].grid(column=4, row=8, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=3,pady=3)

	
		# Object in the bottom frame
	frame_bottom_right.grid_columnconfigure(0, weight=1)
	frame_bottom_right.grid_columnconfigure(1, weight=1)
	frame_bottom_right.grid_columnconfigure(2, weight=1)
	frame_bottom_right.grid_columnconfigure(3, weight=1)
	frame_bottom_right.grid_columnconfigure(4, weight=1)
	master_label.grid(column=0, row=0, columnspan=4, sticky=tk.W+tk.E+tk.N+tk.S)
	master_entry.grid(column=0, row=1, columnspan=4, sticky=tk.W+tk.E+tk.N+tk.S,pady=5)
	host_label.grid(  column=0, row=2, columnspan=4, sticky=tk.W+tk.E+tk.N+tk.S)
	host_entry.grid(  column=0, row=3, columnspan=4, sticky=tk.W+tk.E+tk.N+tk.S,pady=5)
	send_button.grid( column=0, row=4, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=5)
	grip_button.grid( column=2, row=4, columnspan=2, sticky=tk.W+tk.E+tk.N+tk.S,padx=5)
	
	# ***********************
	# Event creation and binding
	# ***********************
	dots = []
	isGripping = [False] # "Hack" to use this as a closure it needs to be mutable or use nonlocal keyword in python 3.x

	def sendCmd():
		if isGripping[0]:
			C = SETTINGS["grip_pressed"]
		else:
			C = SETTINGS["grip_released"]
		
		param = "x={},y={},z={} A=0,B=0,C={}".format(x_entry.get(), y_entry.get(), z_slider.get(),C)
		
		settings = SETTINGS
		settings["ROS_MASTER_URI"] = master_entry.get()
		settings["ROS_HOSTNAME"]   = host_entry.get()
		rosrun(settings,param)

	def sendCmd2(x,y,z,theta1,theta2,grip):
		param = "x={},y={},z={} A={},B={},C={}".format(x,y,z,theta1,theta2,grip)
		
		settings = SETTINGS
		settings["ROS_MASTER_URI"] = master_entry.get()
		settings["ROS_HOSTNAME"]   = host_entry.get()
		rosrun(settings,param)


	def canvas_erase(event):
		for dot in dots:
			drawing_area.delete(dot)

	def canvas_click(event):
		# GUI --------------------
		dots.append(drawing_area.create_oval(event.x, event.y, event.x, event.y,width=SETTINGS["dot_radius"],outline=SETTINGS["dot_color"]))
		x_entry.delete(0,tk.END)
		x_entry.insert(0,-1*(int(SETTINGS["canvas_width"])/2 - event.x))
		y_entry.delete(0,tk.END)
		y_entry.insert(0,int(SETTINGS["canvas_height"])/2 - (event.y))
		
		#time.sleep(1)

		# App --------------------
		sendCmd()

	def send_button_click():
		sendCmd()

	def grip_button_click():
		isGripping[0] = not isGripping[0]
		if isGripping[0]:
			grip_button.config(bg=SETTINGS["grip_buton_bg_gripped"])
		else:
			grip_button.config(bg=SETTINGS["grip_buton_bg_release"])
		sendCmd()

	for i in range(len(scenar_buttons)):
		# THIS MOTHERFUCKING TRICKS !
		def scenario_button_click(i):
			def scenario_play():
				scenar = scenario_fetch(i)
				for point in scenar:
					sendCmd2(point[0],point[1],point[2],point[3],point[4],point[5])
					time.sleep(1)
			return scenario_play
		scenar_buttons[i].config(command=scenario_button_click(i))	

	
	# Binding
	send_button.config(command=send_button_click)
	grip_button.config(command=grip_button_click)
	drawing_area.bind("<ButtonPress-1>", canvas_click)
	z_slider.config(command=canvas_erase)
	


	# ***********************
	# Start the GUI
	# ***********************
	root.mainloop()

if __name__ == '__main__':
	SETTINGS = loadSettings()
	main()
	
